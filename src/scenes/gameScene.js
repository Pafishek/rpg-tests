import Phaser from "phaser";
import Player from "../objects/player";

class GameScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'GameScene'
    });
  }

  preload() {

  }

  create() {
    const map = this.make.tilemap({key: 'castle_level'});
    const tileset = map.addTilesetImage('castle', 'castle_tiles');
    const bellow_layer = map.createLayer("bellow", tileset, 0, 0); // layer index, tileset, x, y
    const boundaries_layer = map.createLayer("boundaries", tileset, 0, 0); // layer index, tileset, x, y
    const above_layer = map.createLayer("above", tileset, 0, 0); // layer index, tileset, x, y

    bellow_layer.setDepth(0);
    boundaries_layer.setDepth(5);
    above_layer.setDepth(10);

    boundaries_layer.setCollisionByProperty({collides: true});

        this.hero = new Player(400, 300, this, "human_male", {
          equipment: {
            legs: "green_pants",
            feet: "brown_shoes",
            body: "leather_armor",
            head: "chain_hood",
            weapon: "dagger"
          }
        });

        this.hero.sprites_group.setDepth(7);

        this.physics.add.collider(this.hero.physics_body, boundaries_layer);


    this.physics.world.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.physics.world.setBoundsCollision();

    this.cameras.main.setBounds(0, 0, map.widthInPixels, map.heightInPixels);
    this.cameras.main.startFollow(this.hero.physics_body, false);
    this.cameras.main.setZoom(2);

    if (false) {
      // Debug graphics
      // Turn on physics debugging to show player's hitbox
      this.physics.world.createDebugGraphic();

      // Create worldLayer collision graphic above the player, but below the help text

      const graphics = this.add
        .graphics()
        .setAlpha(0.75)
        .setDepth(20);
      boundaries_layer.renderDebug(graphics, {
        tileColor: null, // Color of non-colliding tiles
        collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255), // Color of colliding tiles
        faceColor: new Phaser.Display.Color(40, 39, 37, 255) // Color of colliding face edges
      });
    }
  }

  update(time, delta) {
    this.hero.update();
  }
}

export default GameScene
