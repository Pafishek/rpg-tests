import Phaser from "phaser";
import SpriteLoader from "../utils/spriteLoader";
import logoImg from "../assets/logo.png";
import castle_tiles from "../assets/spritesheets/tiles/castle.png";
import castle_level from "../levels/castle_map.json";
import sound_weapon_dagger_slash from "../assets/sounds/effects/weapon_dagger_slash.ogg";

class LoadingScene extends Phaser.Scene {
  constructor() {
    super({
      key: 'LoadingScene'
    });
  }

  preload() {
    const progress = this.add.graphics();
    const spriteLoader = new SpriteLoader(this);

    this.load.image('logo', logoImg);

    this.load.on('progress', (value) => {
      progress.clear();
      progress.fillStyle(0x0000ff, 1);
      progress.fillRect(5, this.sys.game.config.height - 15, (this.sys.game.config.width - 10) * value, 20);
    });

    // Register a load complete event to launch the title screen when all files are loaded
    this.load.on('complete', () => {
      // prepare animations
      spriteLoader.prepareAnimations();
      progress.destroy();
      this.scene.start('GameScene');
    });

    // preload game assets
    this.load.image('castle_tiles', castle_tiles);
    this.load.audio('weapon_dagger_slash', sound_weapon_dagger_slash);
    this.load.tilemapTiledJSON('castle_level', castle_level);
    spriteLoader.loadSprites();
  }

  create() {
    const logo = this.add.image(this.sys.game.config.width / 2, this.sys.game.config.height / 2, 'logo');
  }
}

export default LoadingScene
