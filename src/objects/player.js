import Character from "./character";
import Phaser from "phaser";

class Player extends Character {
  constructor(x, y, scene, sprite, params) {
    super(x, y, scene, sprite, params);
    this.cursors = scene.input.keyboard.createCursorKeys();
    this.action_keys = scene.input.keyboard.addKeys('ctrl');
    this.sounds = {
      dagger_slash: scene.sound.add('weapon_dagger_slash')
    }
  }

  update(args) {
    if (typeof args === "undefined") args = {};
    // Stop any previous movement from the last frame
    this.physics_body.setVelocity(0, 0);

    this.action = "idle";

    if (this.action_keys.ctrl.isDown) {
      this.action = "slash";
      this.can_move = false;
      args.prevent_change = true;
      let _this = this;
      this.sprites.main.on(Phaser.Animations.Events.ANIMATION_START, function () {
        if (_this.action_keys.ctrl.isDown) {
          _this.sounds.dagger_slash.play({delay: 0.2});
        }
      });
    }

    // Vertical movement
    if (this.cursors.up.isDown) {
      this.direction = "up";
      if (this.can_move) {
        this.action = "walking";
        this.physics_body.setVelocityY(-100);
      }
    } else if (this.cursors.down.isDown) {
      this.direction = "down";
      if (this.can_move) {
        this.action = "walking";
        this.physics_body.setVelocityY(100);
      }
    }

    // Horizontal movement
    if (this.cursors.left.isDown) {
      this.direction = "left";
      if (this.can_move) {
        this.action = "walking";
        this.physics_body.setVelocityX(-100);
      }
    } else if (this.cursors.right.isDown) {
      this.direction = "right";
      if (this.can_move) {
        this.action = "walking";
        this.physics_body.setVelocityX(100);
      }
    }

    super.update(args);
  }
}

export default Player
