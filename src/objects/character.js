import Phaser from "phaser";

class Character extends Phaser.GameObjects.GameObject {
  constructor(x, y, scene, sprite = "human_male", params = {}) {
    super(scene, sprite);
    this.id = "character_" + Math.random();
    this.direction = params.direction ? params.direction : "down";
    this.action = "idle";
    this.can_change = true;
    this.can_move = true;
    this.sprite = sprite;
    this.slots = ["legs", "feet", "body", "head", "weapon"] // main sprite is added automatically, so we can change slots in extensions

    this.equipment = {};
    for (let slot of this.slots) {
      this.equipment[slot] = (params.equipment && params.equipment[slot]) ? params.equipment[slot] : null;
    }

    this.sprites_group = scene.add.group();
    this.sprites = {};
    this.physics_body = scene.physics.add.sprite(x, y, null)
      .setSize(24, 8)
      .setOffset(4, 38)
      .setCollideWorldBounds()
      .setVisible(false);
    for (let slot of ["main"].concat(this.slots)) {
      this.sprites[slot] = scene.add.sprite(x, y, null)
        .setSize(24, 16);
      this.sprites_group.add(this.sprites[slot]);
    }

    window.characters = window.characters || {};
    window.characters[this.id] = this.equipment;

    scene.add.existing(this)
  }

  update_sprites(context, sequence) {
    if (typeof sequence !== "object") {
      throw new Error("Character: can't update equipment, sequence is not an array")
    }
    context.sprites.main.play(context.sprite + '_' + context.action + '_' + context.direction, true);
    context.sprites.main.x = context.physics_body.x;
    context.sprites.main.y = context.physics_body.y;
    sequence.forEach(function (equipment_slot) {
      let sprite = context.sprites[equipment_slot];
      let equipment = context.equipment[equipment_slot];
      if (equipment === null) {
        sprite.setActive(false).setVisible(false)
      } else {
        sprite.setActive(true).setVisible(true).play('equipment_' + equipment_slot + '_' + equipment + '_' + context.action + '_' + context.direction, true);
      }
      sprite.x = context.physics_body.x;
      sprite.y = context.physics_body.y;
    });
  }

  update(args) {
    if (this.can_change) {
      this.sprites.main.play(this.sprite + '_' + this.action + "_" + this.direction, true);
      this.update_sprites(this, this.slots);
    }
    if (args.prevent_change) {
      let self = this;
      this.can_change = false;
      this.can_move = false;
      this.sprites.main.on(Phaser.Animations.Events.ANIMATION_COMPLETE, function(){
        self.can_change = true;
        self.can_move = true
        args.prevent_change = false;
      }, this.scene);
    }
  }
}

export default Character
