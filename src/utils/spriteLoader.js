import {human_sprites, human_defaults} from '../manifests/human_sprites.json'
import {enemy_sprites, enemy_defaults} from '../manifests/enemy_sprites.json'
import {equipment_sprites, equipment_defaults} from '../manifests/equipment_sprites.json'

class SpriteLoader {
  constructor(context) {
    this.context = context;
    this.config = {
      manifests: [
        {
          prefix: "human",
          definitions: human_sprites,
          defaults: human_defaults,
          frameWidth: 64,
          frameHeight: 64
        },
        {
          prefix: "enemy",
          definitions: enemy_sprites,
          defaults: enemy_defaults,
          frameWidth: 64,
          frameHeight: 64
        },
        {
          prefix: "equipment",
          definitions: equipment_sprites,
          defaults: equipment_defaults,
          frameWidth: 64,
          frameHeight: 64
        }
      ]
    }
  }

  getBaseDefinitions(manifest) {
    let sprites = [];

    // assign default values from JSON
    if (!manifest.defaults.hasOwnProperty("frames") || !manifest.defaults.hasOwnProperty("single_frame") || !manifest.defaults.hasOwnProperty("directions")) {
      throw new Error("spriteLoader: manifest missing default property" + JSON.stringify(manifest));
    }
    const defaults = manifest.defaults;

    // iterate over manifest definitions
    let name_key;
    for (name_key in manifest.definitions) {
      if (!manifest.definitions.hasOwnProperty(name_key)) {
        throw new Error("spriteLoader: manifest missing property" + name_key + " " + JSON.stringify(manifest.definitions));
      }
      let object = manifest.definitions[name_key]

      object.forEach(function (action) {
        // check required properties name and filename
        if (!action.hasOwnProperty("name") || !action.hasOwnProperty("filename")) {
          console.log(object);
          throw new Error("spriteLoader: action definitions are missing \n" + JSON.stringify(action));
        }
        // load default properties
        if (typeof action.frames === 'undefined') {
          action.frames = defaults.frames
        }
        if (typeof action.single_frame === 'undefined') {
          action.single_frame = defaults.single_frame
        }
        if (typeof action.frame_rate === 'undefined') {
          action.frame_rate = defaults.frame_rate
        }
        if (typeof action.directions === 'undefined') {
          action.directions = defaults.directions
        }
        if (typeof action.repeat === 'undefined') {
          action.repeat = defaults.repeat
        }

        let sprite = {
          name: manifest.prefix + "_" + name_key + "_" + action.name,
          frames: action.frames,
          repeat: action.repeat,
          single_frame: action.single_frame,
          frame_rate: action.frame_rate,
          directions: action.directions,
          path: action.filename,
        };

        sprites.push(sprite);
      })
    }
    // return array of object names ["prefix_name_action", ...]
    return sprites;
  }

  prepareAnimations() {
    for (let manifest of this.config.manifests) {
      let sprites = this.getBaseDefinitions(manifest);
      for (let sprite of sprites) {
        if (!sprite.hasOwnProperty("directions") || sprite.directions.length <= 0) {
          console.log(sprite);
          throw new Error("spriteLoader: sprite definitions are missing directions \n" + JSON.stringify(sprite));
        }
        let directions = sprite.directions;
        let frame_count = sprite.frames;
        let starting_index = 0;


        for (let direction of directions) {
          let frames;
          if (sprite.single_frame !== false) {
            frames = this.context.anims.generateFrameNumbers(sprite.name, {
              start: starting_index + sprite.single_frame,
              end: starting_index + sprite.single_frame
            })
          } else {
            frames = this.context.anims.generateFrameNumbers(sprite.name, {
              start: starting_index,
              end: starting_index + frame_count - 1
            })
          }

          this.context.anims.create({
            key: sprite.name + "_" + direction,
            frames: frames,
            frameRate: sprite.frame_rate,
            repeat: sprite.repeat
          });
          starting_index += frame_count;
        }
      }
    }
  }

  loadSprites() {
    for (let manifest of this.config.manifests) {
      let sprites = this.getBaseDefinitions(manifest);
      for (let sprite_definition of sprites) {
        let path = require("../assets/spritesheets/" + sprite_definition.path);
        this.context.load.spritesheet(sprite_definition.name, path, {
          frameWidth: manifest.frameWidth,
          frameHeight: manifest.frameHeight
        });
      }
    }
  }
}

export default SpriteLoader

