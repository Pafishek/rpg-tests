import './style.scss';
import Phaser from 'phaser';
import LoadingScene from './scenes/loadingScene';
import GameScene from './scenes/gameScene';

const config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: 800,
  height: 600,
  scene: [
    LoadingScene,
    GameScene
  ],
  render: {
    pixelArt: true,
    antialias: false,
    autoResize: false
  },
  physics: {
    default: "arcade",
    arcade: {
      gravity: {y: 0} // Top down game, so no gravity
    }
  }
};

const game = new Phaser.Game(config);
